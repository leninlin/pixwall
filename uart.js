SerialPort = require("serialport").SerialPort;
var serialPort = new SerialPort("/dev/ttyUSB0", {
  baudrate: 115200,
});

var s = 0;
var maxX = 42;
var maxY = 28;
var buffer = new Buffer(Math.ceil(maxX*maxY/3)+1);

drawWall = function() {
    var pos, state;

    clearBuffers();
    mainData = getData();

    for (var y = 0; y < maxY; y++) {
        for (var x = 0; x < maxX; x++) {
            pos = y*maxX+x;
            state = getStatePixel(pos);
            addPixel(pos, state);
        }
    }

    send(buffer);
}

getData = function() {
    var d = [
        {color: [0,0,255], data: []},
    ];

    for (var y = 0; y < maxY; y++) {
        for (var x = 0; x < maxX; x++) {
            if (x >= s && x <= s+3) {
                d[0].data.push(10);
            } else {
                d[0].data.push(0);
            }
        }
    }
    s++;

    if (s > maxX) {
        s = 0;
    }

    return d;
};

send = function(buffer) {
    writeData(buffer, function(err) {
        setTimeout(function() {
            drawWall();
        }, 40);
    });
}

writeData = function(data, callback) {
    data.writeUInt8(10, Math.ceil(maxX*maxY/3));
    console.log(buffer);
    serialPort.write(data, callback);
}

clearBuffers = function() {
    buffer.fill(0);
}

getStatePixel = function(pos) {
    var opacity = 0;

    for (layer in mainData) {
        opacity = parseInt(mainData[layer].data[pos])/10;
    }

    return opacity > 0.2 ? 1 : 0;
}

var bits = [1, 2, 4];
addPixel = function(pos, state) {
    if (state) {
        var bit = bits[pos%3];
        var bufferByte = buffer.readUInt8(Math.floor(pos/3));
        if (bufferByte < 0x30) {
            bufferByte = 0x30;
        }
        bufferByte = bufferByte + bit;
        buffer.writeUInt8(bufferByte, Math.floor(pos/3));
    } else {
        var bufferByte = buffer.readUInt8(Math.floor(pos/3));
        bufferByte = 0x30;
        buffer.writeUInt8(bufferByte, Math.floor(pos/3));
    }
}

var mainData = getData();

setTimeout(function() {
    drawWall();
}, 2000);