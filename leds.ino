#include <Adafruit_NeoPixel.h>
#include <Wire.h>

#define SLAVE_ADDRESS 0x06
#define PIN 9
#define LEDS 14*14

Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDS, PIN, NEO_GRB + NEO_KHZ800);

uint8_t i, n, k, r=0, g=0, b=0, cmd, final;
uint32_t color = 0;

void setup() {
    Wire.begin(SLAVE_ADDRESS);
    Wire.onReceive(dataEvent);

    strip.begin();
    strip.show();

    pinMode(13, OUTPUT);
}

void loop() {}

void dataEvent(int bytes) {
    i = 0;
    cmd = Wire.read();

    switch (cmd) {
        case 14:
            while (Wire.available()) {
                r = Wire.read();
                g = Wire.read();
                b = Wire.read();
                color = strip.Color(g, r, b);
            }
            break;

        case 15:
            while (Wire.available()) {
                n = Wire.read();
                k = 128;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;

                if (n & k) {
                    strip.setPixelColor(i++, 0, 0, 255);
                } else {
                    strip.setPixelColor(i++, 0, 0, 0);
                }
                k = k/2;
            }

            strip.show();
            break;
    }

}