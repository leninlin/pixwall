global.$ = $;

SerialPort = require("serialport").SerialPort;

var VK = require('vkapi');
var http = require('http');
var fs = require('fs');
var rest = require('restler');

var vk = new VK({
    appID: 4298713,
    appSecret: 'b4sQI2e7R9CLrAFs9wVQ',
    mode: 'oauth'
});

//access_token=698b14fff9964dc375d9adda5712e745da494f1ec7854f29ddeea84a5ce5c498b27454af8ea02ad42334f&expires_in=0&user_id=2098509
vk.setToken({token: '698b14fff9964dc375d9adda5712e745da494f1ec7854f29ddeea84a5ce5c498b27454af8ea02ad42334f'});

var OpenNI = require('openni');

var openni = OpenNI();

var mode = 'people';
var widthCell = 20;
var heightCell = 20;
var maxX = 42;
var maxY = 28;
var maxXBuf = 640;
var maxYBuf = 480;
var cellX = parseInt(maxXBuf / maxX);
var cellY = parseInt(maxYBuf / maxY);
var wall = Array.apply(null, new Array(maxY)).map(function() { return Array.apply(null, new Array(maxX)).map(Number.prototype.valueOf,0)});
var scelet = [];
var opacity = 0;
var minOpacity = 0.2;
var tilt = 0;
var accuracy = 0.0;
var table = [];
var data = {};
var saved = [];
var showSaved = 0;

var maxXInController = maxX/3;
var maxYInController = maxY;
var controllers = [
    {
        buffer: new Buffer(Math.ceil(maxXInController*maxYInController/2)),
        firstRevert: 0,
        send: 0,
        /*serial: new SerialPort("/dev/ttyUSB0", {
          baudrate: 115200,
        }),*/
    },
    {
        buffer: new Buffer(Math.ceil(maxXInController*maxYInController/2)),
        firstRevert: 0,
        send: 0,
        serial: new SerialPort("/dev/ttyUSB0", {
          baudrate: 115200,
        }),
    },
    {
        buffer: new Buffer(Math.ceil(maxXInController*maxYInController/2)),
        firstRevert: 1,
        send: 0,
        serial: new SerialPort("/dev/ttyUSB1", {
          baudrate: 115200,
        }),
    }
];

drawWall = function() {
    var pos, color;

    clearBuffers();
    data = getData();

    for (var y = 0; y < maxY; y++) {
        for (var x = 0; x < maxX; x++) {
            pos = y*maxX+x;
            color = getColor(pos);
            setState(pos, color);

            table[y][x].css('background', 'rgba(' + color.join(',') + ', 1)');
        }
    }

    sendData();
}

getData = function() {
    var result = [];
    if (mode == 'people' || mode == 'play') {
        result.push({color: [255,0,0], data: openni.getUserPixels()});
    }

    if (mode == 'show' || mode == 'play') {
        console.log(showSaved);
        result.push({color: [0,255,0], data: saved[showSaved]});
    }

    if (mode == 'play') {
        var pCount = 0;
        var sCount = 0;
        var xCount = 0;
        for (var i = 0; i < result[0].data.length; i++) {
            var opacity1 = result[0].data.readUInt8(i)/10 > minOpacity ? 1 : 0;
            var opacity2 = result[1].data.readUInt8(i)/10 > minOpacity ? 1 : 0;

            if (opacity1 && opacity2) {
                xCount++;
            } else if (opacity1) {
                pCount++;
            } else if (opacity2) {
                sCount++;
            }
        };

        if (xCount / (xCount+pCount+sCount) > 0.8) {
            vk.request('photos.getWallUploadServer', {gid : 68580685});
            vk.on('done:photos.getWallUploadServer', function(result) {
                var options = {
                    host: '192.168.0.65',
                    port: 80,
                    path: '/image/jpeg.cgi',
                    auth: 'admin:18sen1989',
                }
                http.get(options, function(res) {
                    var imagedata = '';
                    res.setEncoding('binary');

                    res.on('data', function(chunk) {
                        imagedata += chunk;
                    })

                    res.on('end', function() {

                        fs.writeFile('image.jpg', imagedata, 'binary', function(err){
                            rest.post(result.response.upload_url, {
                                multipart: true,
                                data: {
                                    'photo': rest.file(__dirname  + '/image.jpg', null, fs.statSync(__dirname  + '/image.jpg').size, null, 'image/jpg')
                                }
                            }).on('complete', function(data) {
                                data = JSON.parse(data);
                                data['gid'] = 68580685;
                                vk.request('photos.saveWallPhoto', data);
                                vk.on('done:photos.saveWallPhoto', function(result) {
                                    vk.request('wall.post', {owner_id: "-68580685", from_group: 1, message: encodeURIComponent("#leninlin dsfdsf"), attachment: result.response[0].id});
                                    vk.on('done:wall.post', function(result) {
                                        console.log(result);
                                    });
                                });
                            });
                        });
                    });
                    
                });
            });

            var buffer = new Buffer(result[0].data.length);
            buffer.fill(10);
            result = [{color: [255,0,255], data: buffer}];

            mode = 'people';
        }
    }

    return result;
}

saveData = function() {
    saved.push(data[0].data);
}

sendData = function() {
    for (i in controllers) {
        if (!controllers[i].send) {
            writeData(controllers[i].buffer, i, function() {
                controllers[i].send = 1;

                setTimeout(function() {
                    if (allSend()) {
                        drawWall();
                    } else {
                        sendData();
                    }
                }, 40);
            });

            break;
        }
    }
}

writeData = function(data, index, callback) {
    if (controllers[index].serial) {
        controllers[index].serial.write(data, callback);
    } else {
        callback();
    }
}

allSend = function() {
    for (i in controllers) {
        if (!controllers[i].send) {

            return false;
        }
    }    

    return true;
}

clearBuffers = function() {
    for (i in controllers) {
        controllers[i].buffer.fill(0);
        controllers[i].send = 0;
    }
}

getColor = function(pos) {
    var color = [0, 0, 0];
    var opacity = 0;

    for (layer in data) {
        opacity = parseInt(data[layer].data[pos])/10;

        if (opacity > minOpacity) {
            color[0] += data[layer].color[0] ? 255 : 0;
            color[1] += data[layer].color[1] ? 255 : 0;
            color[2] += data[layer].color[2] ? 255 : 0;
        }
    }

    return color;
}

var bits = [128, 64, 32, 16, 8, 4, 2, 1];
setState = function(pos, color) {
    var object = getControllerAndCPosByGPos(pos);
    var index = object['index'];
    var posC = object['pos'];
    var bufferByte = controllers[index].buffer.readUInt8(Math.floor(posC/2));
    var bit = 0;

    for (var i = 0; i < 3; i++) {
        if (color[i]) {
            bit = bits[(posC%2)*4+i];
            bufferByte = bufferByte | bit;
        }
    }

    if (posC == maxYInController*maxXInController-1) {
        bit = bits[3] + bits[7];
        bufferByte = bufferByte | bit;
    }

    controllers[index].buffer.writeUInt8(bufferByte, Math.floor(posC/2));
}

getControllerAndCPosByGPos = function(pos) {
    var y = Math.floor(pos/maxX);
    var x = pos - y*maxX;
    var index = Math.floor(x/maxXInController);
    var xc = x - index*maxXInController;

    if (y % 2 == controllers[index].firstRevert) {
        xc = maxXInController-1 - xc;
    }

    return {index: index, pos: y*maxXInController+xc};
}

setKinectEvents = function() {
    ['newuser','userexit','lostuser','posedetected','calibrationstart','calibrationsucceed','calibrationfail',]
        .forEach(function(eventType) {
            openni.on(eventType, function(userId) {
                console.log('User %d emitted event: %s', userId, eventType);
            });
        });

    ['head','neck','torso','waist','left_collar','left_shoulder','left_elbow','left_wrist','left_hand',
     'left_fingertip','right_collar','right_shoulder','right_elbow','right_wrist','right_hand','right_fingertip',
     'left_hip','left_knee','left_ankle','left_foot','right_hip','right_knee','right_ankle','right_foot']
        .forEach(function(jointName) {
            openni.on(jointName, function(user, x, y, z) {
                if (!scelet[jointName]) {
                    scelet[jointName] = {
                        xReal: 0,
                        yReal: 0,
                        zReal: 0,
                    };
                }

                scelet[jointName].xReal = x/5;
                scelet[jointName].yReal = -y/5;
                scelet[jointName].zReal = z;
                
                scelet[jointName].x = parseInt(maxX/2 + scelet[jointName].xReal/cellX);
                scelet[jointName].y = parseInt(maxY/2 + scelet[jointName].yReal/cellY);
            });
        });
}

drawTable = function() {
    var content = "<table>";

    for (var y = 0; y < maxY; y++) {
        content += "<tr>";
        for (var x = 0; x < maxX; x++) {
            content += "<td><div></div></td>";
        }
        content += "</tr>";
    }
    content += "</table>";
    $('.pixels').html(content);

    table = [];
    $('.pixels table tr').each(function(k, tr) {
        $(tr).find('td').each(function(t, td) {
            if (!table[k]) {
                table[k] = [];
            }
            table[k][t] = $('div', td);
            table[k][t].css({
                width: widthCell,
                height: heightCell,
                'font-size': '12px',
            });
        });
    });

}

setKeyEvents = function() {
    $(window).keydown(function(event) {
        console.log(event.which);
        if (event.which == 187) {
            if (accuracy < 1) {
                accuracy += 0.05;
            }
        }
        if (event.which == 189) {
            if (accuracy > 0) {
                accuracy -= 0.05;
            }
        }
        if (event.which == 38) {
            if (tilt < 100) {
                tilt++;
                context.tilt(tilt);
            }
        }
        if (event.which == 40) {
            if (tilt > -100) {
                tilt--;
                context.tilt(tilt);
            }
        }

        // key S
        if (event.which == 83) {
            saveData();
        }

        // key M
        if (event.which == 77) {
            switch (mode) {
                case 'people':
                    mode = 'show';
                    showSaved = Math.round(Math.random() * (saved.length-1));
                    break;
                case 'show':
                    mode = 'play';
                    break;
                case 'play':
                    mode = 'people';
                    break;
            }
        }
    });
}

$(document).ready(function() {
    setKinectEvents();
    drawTable();
    setKeyEvents();

    drawWall();
});