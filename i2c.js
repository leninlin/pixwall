GPIO = require('rpi').GPIO;
rasp2c = require('rasp2c');
io = require('socket.io-client');

intervals = {};
addEventInterval = function(id, func, interval) {
    if (intervals[id]) {
        clearInterval(intervals[id]);
    }
    intervals[id] = setInterval(func, interval);
}
removeEventInterval = function(id) {
    if (intervals[id]) {
        clearInterval(intervals[id]);
    }
}

socket = io.connect('http://10.42.0.1:3000');

socketConnect = function () {
    socket.socket.connect();
}

addEventInterval('socketConnect', socketConnect, 5000);

socket.on('connect', function() {
    removeEventInterval('socketConnect');

    socket.on('data', function(data) {
        setData(data);
    });

    socket.on('disconnect', function() {
        addEventInterval('socketConnect', socketConnect, 5000);
    });
});

var s = 0;
var maxX = 42;
var maxY = 28;
var map =  [
    [3, 4, 5],
    [6, 7, 8],
];
var maxXInController = maxX/map[0].length;
var maxYInController = maxY/map.length;
var controllers = {
    3: {
        full: new Buffer(maxXInController*maxYInController*3),
        bits: new Buffer(Math.ceil(maxXInController*maxYInController/8)),
        change: {
            buffer: new Buffer(maxXInController*maxYInController*3),
            length: 0,
        },
        firstRevert: 0,
        send: 0,
        color: [0,0,0],
        reset: new GPIO(10, 'out'),
    },
    4: {
        full: new Buffer(maxXInController*maxYInController*3),
        bits: new Buffer(maxXInController*maxYInController/8),
        change: {
            buffer: new Buffer(maxXInController*maxYInController*3),
            length: 0,
        },
        firstRevert: 0,
        send: 0,
        color: [0,0,0],
        reset: new GPIO(9, 'out'),
    },
    5: {
        full: new Buffer(maxXInController*maxYInController*3),
        bits: new Buffer(maxXInController*maxYInController/8),
        change: {
            buffer: new Buffer(maxXInController*maxYInController*3),
            length: 0,
        },
        firstRevert: 1,
        send: 0,
        color: [0,0,0],
        reset: new GPIO(11, 'out'),
    },
    6: {
        full: new Buffer(maxXInController*maxYInController*3),
        bits: new Buffer(Math.ceil(maxXInController*maxYInController/8)),
        change: {
            buffer: new Buffer(maxXInController*maxYInController*3),
            length: 0,
        },
        firstRevert: 0,
        send: 0,
        color: [0,0,0],
        reset: new GPIO(25, 'out'),
    },
    7: {
        full: new Buffer(maxXInController*maxYInController*3),
        bits: new Buffer(maxXInController*maxYInController/8),
        change: {
            buffer: new Buffer(maxXInController*maxYInController*3),
            length: 0,
        },
        firstRevert: 0,
        send: 0,
        color: [0,0,0],
        reset: new GPIO(7, 'out'),
    },
    8: {
        full: new Buffer(maxXInController*maxYInController*3),
        bits: new Buffer(maxXInController*maxYInController/8),
        change: {
            buffer: new Buffer(maxXInController*maxYInController*3),
            length: 0,
        },
        firstRevert: 1,
        send: 0,
        color: [0,0,0],
        reset: new GPIO(8, 'out'),
    },
}
var bytesPerLed = [2,3,4];
var maxBufLength = 31;

drawWall = function() {
    clearBuffers();
    mainData = getData();
    var color = getColor(false);
    setColor(color);
    var cmd = getCommand(color);
    var pos, state;

    for (var y = 0; y < maxY; y++) {
        for (var x = 0; x < maxX; x++) {
            pos = y*maxX+x;
            //var color = getColor(pos);
            //setPixel(pos, color, cmd);
            state = getStatePixel(pos);
            addPixel(pos, state, cmd);
        }
    }

        var bpl = getBytesPerLed(cmd);
        var maxLength = Math.floor(maxBufLength/bpl)*bpl;
        for (i in controllers) {
            //send(controllers[i].change.buffer.slice(0, controllers[i].change.length), maxLength, cmd, i);
            send(controllers[i].bits, maxLength, cmd, i);
            break;
        }
}

getData = function() {
    var d = [
        {color: [0,0,255], data: []},
    ];

    for (var y = 0; y < maxY; y++) {
        for (var x = 0; x < maxX; x++) {
            if (y >= s && y <= s+3) {
                d[0].data.push(10);
            } else {
                d[0].data.push(0);
            }
        }
    }
    s++;

    if (s > maxY) {
        s = 0;
    }

    return d;

//    return mainData;
};

setData = function(newData) {
    if (mainData == {}) {
        mainData = newData;
        drawWall();
    } else {
        mainData = newData;
    }
};

send = function(buffer, maxLength, cmd, conn) {
    console.log(buffer);
    writeData(buffer, 15, conn, function(err) {
        controllers[conn].send = 1;
        if (allSend()) {
            rasp2c.detect(function(err, result) {
                console.log(result);
                for (i in controllers) {
                    var detect = false;
                    for (k in result) {
                        if (result[k] == i) {
                            detect = true;
                        }
                    }
                    if (!detect) {
                        resetController(i);
                    }
                }
                drawWall();
            });
        } else {
            for (i in controllers) {
                if (!controllers[i].send) {
                    send(controllers[i].bits, maxLength, cmd, i);
                    break;
                }
            }
        }
    });
}

writeData = function(data, cmd, conn, callback) {
    var strData = '';
    for (var i=0;i<data.length;i++) {
        strData += '0x' + data.readUInt8(i).toString(16) + ' ';
    }
    rasp2c.set(conn, '0x'+cmd.toString(16), strData.length ? strData + 'i' : '', callback);
}

readData = function(cmd, conn, callback) {
    rasp2c.dump(conn, '0x'+cmd.toString(16)+'-'+'0x'+cmd.toString(16), callback);
}

allSend = function() {
    for (i in controllers) {
        if (!controllers[i].send) {

            return false;
        }
    }    

    return true;
}

clearBuffers = function() {
    for (i in controllers) {
        controllers[i].bits.fill(0);
        /*controllers[i].change.buffer.fill(0);
        controllers[i].change.length = 0;*/
        controllers[i].send = 0;
    }
}

getCommand = function(color) {
    cmd = 0;
    cmd += color[0] ? 1 : 0;
    cmd += color[1] ? 2 : 0;
    cmd += color[2] ? 4 : 0;

    return cmd;
}

getColor = function(pos) {
    var color = [0, 0, 0];
    var opacity = 1;

    for (layer in mainData) {
        if (pos !== false) {
            opacity = parseInt(mainData[layer].data[pos])/10;
        }

        if (opacity > 0.2 || pos === false) {
            color[0] += mainData[layer].color[0] * opacity;
            color[1] += mainData[layer].color[1] * opacity;
            color[2] += mainData[layer].color[2] * opacity;
        }
    }

    return color;
}

setPixel = function(pos, color, cmd) {
    var object = getControllerAndCPosByGPos(pos);
    var id = object['controllerId'];
    var curPos = object['curPos'];

    if (controllers[id].full.readUInt8(curPos*3) != color[0]
        || controllers[id].full.readUInt8(curPos*3+1) != color[1]
        || controllers[id].full.readUInt8(curPos*3+2) != color[2]
    ) {
        controllers[id].change.buffer.writeUInt8(curPos, controllers[id].change.length++);
        switch (cmd) {
            case 1:
            case 2:
            case 4:
                controllers[id].change.buffer.writeUInt8(parseInt(color[parseInt(cmd/2)]), controllers[id].change.length++);
                break

            case 3:
            case 5:
            case 6:
                if (cmd == 3 || cmd == 5) {
                    controllers[id].change.buffer.writeUInt8(parseInt(color[0]), controllers[id].change.length++);
                    controllers[id].change.buffer.writeUInt8(parseInt(color[parseInt((cmd-1)/2)]), controllers[id].change.length++);
                } else {
                    controllers[id].change.buffer.writeUInt8(parseInt(color[1]), controllers[id].change.length++);
                    controllers[id].change.buffer.writeUInt8(parseInt(color[2]), controllers[id].change.length++);
                }
                break;

            case 4:
                controllers[id].change.buffer.writeUInt8(parseInt(color[0]), controllers[id].change.length++);
                controllers[id].change.buffer.writeUInt8(parseInt(color[1]), controllers[id].change.length++);
                controllers[id].change.buffer.writeUInt8(parseInt(color[2]), controllers[id].change.length++);
        }

        controllers[id].full.writeUInt8(parseInt(color[0]), curPos*3);
        controllers[id].full.writeUInt8(parseInt(color[1]), curPos*3+1);
        controllers[id].full.writeUInt8(parseInt(color[2]), curPos*3+2);
    }

}

getControllerAndCPosByGPos = function(pos) {
    yp = parseInt(pos/maxX);
    xp = pos - yp*maxX;

    ym = parseInt(yp/maxYInController);
    xm = parseInt(xp/maxXInController);

    yc = yp - ym*maxYInController;
    xc = xp - xm*maxXInController;

    if (yc % 2 == controllers[map[ym][xm]].firstRevert) {
        xc = maxXInController-1 - xc;
    }

    return {controllerId: map[ym][xm], curPos: yc*maxYInController+xc};
}

getBytesPerLed = function(cmd) {
    var bpl = 1;
    switch (cmd) {
        case 1:
        case 2:
        case 4:
            bpl = bytesPerLed[0];
            break;

        case 3:
        case 5:
        case 6:
            bpl = bytesPerLed[1];
            break;

        case 7:
            bpl = bytesPerLed[2];
            break;
    }

    return bpl;
}

getStatePixel = function(pos) {
    var opacity = 0;

    for (layer in mainData) {
        opacity = parseInt(mainData[layer].data[pos])/10;
    }

    return opacity > 0.2 ? 1 : 0;
}

var bits = [128, 64, 32, 16, 8, 4, 2, 1];
addPixel = function(pos, state, cmd) {
    var object = getControllerAndCPosByGPos(pos);
    var id = object['controllerId'];
    var curPos = object['curPos'];

    if (state) {
        var bit = bits[curPos%8];
        var bufferByte = controllers[id].bits.readUInt8(Math.floor(curPos/8));
        bufferByte = bufferByte | bit;
        controllers[id].bits.writeUInt8(bufferByte, Math.floor(curPos/8));
    }
}

setColor = function(color) {
    for (i in controllers) {
        if (controllers[i].color[0] != color[0]
            || controllers[i].color[1] != color[1]
            || controllers[i].color[2] != color[2]
        ) {
            controllers[i].color = color;
            var buf = new Buffer(color);
            rasp2c.detect(function(err, result) {
                writeData(buf, 14, i, function() {});
            });
            console.log(buf);
        }
    }
}

resetController = function(address) {
    controllers[address].reset.low(function() {
        controllers[address].reset.high();
    })
}

var mainData = getData();

for (i in controllers) {
    controllers[i].full.fill(0);
}

setTimeout(function() {
    drawWall();
}, 2000);